package com.example.nycschools;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.adapter.SchoolAdapter;
import com.example.nycschools.adapter.SchoolDiffUtil;
import com.example.nycschools.model.School;
import com.example.nycschools.repo.SchoolsRepositoryImpl;
import com.example.nycschools.viewmodel.SchoolsViewModel;

import java.util.List;

public class SchoolListActivity extends AppCompatActivity {
    private SchoolAdapter mAdapter;
    private SchoolsViewModel mSchoolsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);


        // Get the view model
        ViewModelProvider provider = new ViewModelProvider(this);
        mSchoolsViewModel = provider.get(SchoolsViewModel.class);
        mSchoolsViewModel.init(new SchoolsRepositoryImpl(getApplication()));

        mSchoolsViewModel.getSchools().observe(this, new Observer<List<School>>() {
            @Override
            public void onChanged(List<School> schools) {
                mAdapter.submitList(schools);
            }
        });
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mAdapter = new SchoolAdapter(new SchoolDiffUtil());
        recyclerView.setAdapter(mAdapter);
    }
}
