package com.example.nycschools.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.nycschools.repo.SchoolsRepository;
import com.example.nycschools.model.School;

import java.util.List;

public class SchoolsViewModel extends AndroidViewModel {

    private LiveData<List<School>> mSchools;
    private SchoolsRepository mRepository;

    public SchoolsViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(SchoolsRepository repo) {
        mRepository = repo;
        mSchools = mRepository.getSchools();
        mRepository.fetchSchoolsData();
    }

    public LiveData<List<School>> getSchools() {
        return mSchools;
    }
}
