package com.example.nycschools.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;


@Dao
public interface SchoolDao {
    @Query("SELECT COUNT(*) FROM school_table")
    int count();

    @Query("SELECT * FROM school_table WHERE dbn = :id ")
    LiveData<School> getSchool(String id);

    @Insert
    void insert(School school);

    @Query("DELETE FROM school_table")
    void deleteAll();

    @Query("SELECT * from school_table ORDER BY school_name ASC")
    LiveData<List<School>> getAllSchools();
}
