package com.example.nycschools.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.example.nycschools.model.School;

public class SchoolDiffUtil extends DiffUtil.ItemCallback<School> {

    @Override
    public boolean areItemsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return oldItem == newItem;
    }

    @Override
    public boolean areContentsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return oldItem.getSchoolName().equals(newItem.getSchoolName());
    }
}
