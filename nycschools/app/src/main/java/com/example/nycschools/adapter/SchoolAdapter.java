package com.example.nycschools.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.SchoolDetailActivity;
import com.example.nycschools.SchoolDetailFragment;
import com.example.nycschools.R;
import com.example.nycschools.model.School;

public class SchoolAdapter extends ListAdapter<School, SchoolAdapter.ViewHolder> {

    public SchoolAdapter(@NonNull SchoolDiffUtil diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_list_content, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolAdapter.ViewHolder holder, int position) {
        final School item = getItem(position);
        holder.bind(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, SchoolDetailActivity.class);
                intent.putExtra(SchoolDetailFragment.ARG_ITEM_ID, item.getDbn());
                context.startActivity(intent);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mCity;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.title);
            mCity = itemView.findViewById(R.id.city);
        }

        void bind(School school) {
            mTitle.setText(school.getSchoolName());
            mCity.setText(school.getCity());
        }
    }
}
