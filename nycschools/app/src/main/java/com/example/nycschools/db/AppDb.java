package com.example.nycschools.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.nycschools.model.SatResult;
import com.example.nycschools.model.SatResultDao;
import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolDao;

@Database(entities = {School.class, SatResult.class}, version = 1)
public abstract class AppDb extends RoomDatabase {
    public abstract SchoolDao schoolDao();

    public abstract SatResultDao satResultDao();


    private static volatile AppDb INSTANCE = null;

    public static AppDb getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDb.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDb.class, "nyc_schools_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
