package com.example.nycschools.repo;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.nycschools.data.NycSchoolsApiSvc;
import com.example.nycschools.db.AppDb;
import com.example.nycschools.model.SatResult;
import com.example.nycschools.model.SatResultDao;
import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolDao;

import java.util.List;

import retrofit2.Call;

public class SchoolsRepositoryImpl implements SchoolsRepository {
    private AppDb mDb;
    private SchoolDao mSchoolDao;
    private SatResultDao mSatResultDao;
    private LiveData<List<School>> mSchools;

    public SchoolsRepositoryImpl(Application application) {
        mDb = AppDb.getDatabase(application);
        mSchoolDao = mDb.schoolDao();
        mSatResultDao = mDb.satResultDao();
        mSchools = mSchoolDao.getAllSchools();
    }

    private static NycSchoolsApiSvc.NycSchoolsApi mApiIfc = NycSchoolsApiSvc.getApiInstance();

    @Override
    public void fetchSchoolsData() {
        new PopulateDb(mDb, this).execute();
    }

    @Override
    public LiveData<School> getSchool(String id) {
        return mSchoolDao.getSchool(id);
    }

    @Override
    public LiveData<List<School>> getSchools() {
        return mSchools;
    }

    @Override
    public LiveData<List<SatResult>> getSatResults() {
        return mSatResultDao.getAll();
    }

    @Override
    public LiveData<SatResult> getSatResult(String id) {
        return mSatResultDao.getSatResult(id);
    }


    private List<School> fetchSchools() {
        Call<List<School>> schoolsReq = mApiIfc.getSchools("s3k6-pzi2.json");

        try {
            return schoolsReq.execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<SatResult> fetchSatResults() {
        Call<List<SatResult>> satResultsReq = mApiIfc.getSatResults("f9bf-2cp4.json");

        try {
            return satResultsReq.execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static class PopulateDb extends AsyncTask<Void, Void, Void> {
        private final SchoolDao mSchoolDao;
        private final SatResultDao mSatResultDao;
        private final SchoolsRepositoryImpl mRepo;

        PopulateDb(AppDb db, SchoolsRepositoryImpl repo) {
            mSchoolDao = db.schoolDao();
            mSatResultDao = db.satResultDao();
            mRepo = repo;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            if (mSchoolDao.count() == 0) {
                populateSchools(mRepo.fetchSchools());
            }

            if (mSatResultDao.count() == 0) {
                populateSatResults(mRepo.fetchSatResults());
            }
            return null;
        }

        private void populateSchools(List<School> schools) {
            if (schools == null) return;
            for (School school : schools) {
                mSchoolDao.insert(school);
            }
        }

        private void populateSatResults(List<SatResult> satResults) {
            if (satResults == null) return;
            for (SatResult satResult : satResults) {
                mSatResultDao.insert(satResult);
            }
        }
    }
}
