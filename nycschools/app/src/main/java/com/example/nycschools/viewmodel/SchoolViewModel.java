package com.example.nycschools.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.nycschools.repo.SchoolsRepository;
import com.example.nycschools.model.SatResult;
import com.example.nycschools.model.School;

public class SchoolViewModel extends AndroidViewModel {

    private LiveData<School> mSchool;
    private LiveData<SatResult> mSatResult;

    private SchoolsRepository mRepository;

    public SchoolViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(SchoolsRepository repo) {
        mRepository = repo;
    }

    public LiveData<School> getSchool(String id) {
        mSchool = mRepository.getSchool(id);
        return mSchool;
    }

    public LiveData<SatResult> getSatResult(String id) {
        mSatResult = mRepository.getSatResult(id);
        return mSatResult;
    }
}
