package com.example.nycschools;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.nycschools.model.SatResult;
import com.example.nycschools.model.School;
import com.example.nycschools.viewmodel.SchoolViewModel;
import com.example.nycschools.repo.SchoolsRepositoryImpl;

public class SchoolDetailFragment extends Fragment {
    public static final String ARG_ITEM_ID = "item_id";
    private String mSchoolId;
    private SchoolViewModel mSchoolViewModel;

    public SchoolDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ARG_ITEM_ID)) {
            mSchoolId = getArguments().getString(ARG_ITEM_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.school_detail, container, false);

        // Get the view model
        ViewModelProvider provider = new ViewModelProvider(this);
        mSchoolViewModel = provider.get(SchoolViewModel.class);
        mSchoolViewModel.init(new SchoolsRepositoryImpl(getActivity().getApplication()));

        LiveData<School> school = mSchoolViewModel.getSchool(mSchoolId);

        if (school != null)
            school.observe(this, new Observer<School>() {
                @Override
                public void onChanged(School school) {
                    if (school != null) {
                        ((TextView) rootView.findViewById(R.id.school_name)).setText(school.getSchoolName());
                        ((TextView) rootView.findViewById(R.id.overview)).setText(school.getOverviewParagraph());
                        ((TextView) rootView.findViewById(R.id.address)).setText(school.getAddress());
                    }
                }
            });

        LiveData<SatResult> satResult = mSchoolViewModel.getSatResult(mSchoolId);

        if (satResult != null)
            satResult.observe(this, new Observer<SatResult>() {
                @Override
                public void onChanged(SatResult satResult) {
                    if (satResult != null) {
                        rootView.findViewById(R.id.sat_results_not_available).setVisibility(View.GONE);
                        ((TextView) rootView.findViewById(R.id.sat_math_avg_score))
                                .setText(
                                        getString(R.string.math_avg_score, satResult.getSatMathAvgScore())
                                );
                        ((TextView) rootView.findViewById(R.id.sat_writing_avg_score))
                                .setText(
                                        getString(R.string.writing_avg_score, satResult.getSatWritingAvgScore())
                                );
                        ((TextView) rootView.findViewById(R.id.sat_critical_reading_avg_score))
                                .setText(
                                        getString(R.string.critical_reading_avg_score, satResult.getSatCriticalReadingAvgScore())
                                );
                    } else {
                        rootView.findViewById(R.id.sat_results_not_available).setVisibility(View.VISIBLE);
                    }
                }
            });

        return rootView;
    }
}
