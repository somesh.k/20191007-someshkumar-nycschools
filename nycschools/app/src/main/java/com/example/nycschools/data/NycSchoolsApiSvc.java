package com.example.nycschools.data;

import com.example.nycschools.model.SatResult;
import com.example.nycschools.model.School;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public final class NycSchoolsApiSvc {

    private static final String API_URL = "https://data.cityofnewyork.us/";

    public interface NycSchoolsApi {
        @GET("resource/{id}/")
        Call<List<School>> getSchools(@Path("id") String id);

        @GET("resource/{id}/")
        Call<List<SatResult>> getSatResults(@Path("id") String id);
    }

    public static NycSchoolsApi getApiInstance() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(httpClient.build())
                .build();

        // Create an instance of our NycSchoolsApi interface.
        return retrofit.create(NycSchoolsApi.class);
    }
}

