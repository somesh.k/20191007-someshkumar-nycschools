package com.example.nycschools.repo;

import androidx.lifecycle.LiveData;

import com.example.nycschools.model.SatResult;
import com.example.nycschools.model.School;

import java.util.List;

public interface SchoolsRepository {
    void fetchSchoolsData();

    LiveData<School> getSchool(String id);

    LiveData<List<School>> getSchools();

    LiveData<List<SatResult>> getSatResults();

    LiveData<SatResult> getSatResult(String id);
}
