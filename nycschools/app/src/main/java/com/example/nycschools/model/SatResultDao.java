package com.example.nycschools.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SatResultDao {
    @Query("SELECT COUNT(*) FROM sat_results_table")
    int count();

    @Query("SELECT * FROM sat_results_table WHERE dbn = :id ")
    LiveData<SatResult> getSatResult(String id);

    @Insert
    void insert(SatResult result);

    @Query("DELETE FROM sat_results_table")
    void deleteAll();

    @Query("SELECT * from sat_results_table ORDER BY school_name ASC")
    LiveData<List<SatResult>> getAll();
}
